import java.util.Scanner;
public class Main {

	//Begin Main Function
	public static void main(String[] args) {

		int choice = -1;
		int again = 0;
		
		//Create the initial Tree T
		BinarySearchTree<Integer> T = new BinarySearchTree<Integer>(new Integer(30));
		
		//Populate the initial tree T
		insert(T, new BinarySearchTree<Integer>(new Integer(10)));
		insert(T, new BinarySearchTree<Integer>(new Integer(45)));
		insert(T, new BinarySearchTree<Integer>(new Integer(38)));
		insert(T, new BinarySearchTree<Integer>(new Integer(20)));
		insert(T, new BinarySearchTree<Integer>(new Integer(50)));
		insert(T, new BinarySearchTree<Integer>(new Integer(25)));
		insert(T, new BinarySearchTree<Integer>(new Integer(33)));
		insert(T, new BinarySearchTree<Integer>(new Integer(8)));
		insert(T, new BinarySearchTree<Integer>(new Integer(12)));
		
		//Create an input scanner
		Scanner input = new Scanner(System.in);
		
		//Begin Menu
		while(again == 0)
		{
			//Display options
			System.out.println("Enter 1 to add a new element to the tree.");
			System.out.println("Enter 2 to search for an element in the tree.");
			System.out.println("Enter 3 to delete an element from the tree.");
			System.out.println("Enter 4 to perform a pre order traversal on the tree.");
			System.out.println("Enter 5 to perform a in order traversal on the tree.");
			System.out.println("Enter 6 to perform a post order traversal on the tree.");
			System.out.println("Enter 7 to exit.");
			System.out.println("Choice: ");
			
			//Take Input
			choice = input.nextInt();
			
			switch (choice)
			{
			case 1:
			{
				System.out.println("Enter a number: ");
				
				int insertInteger = input.nextInt();
				insert(T, new BinarySearchTree<Integer>(new Integer(insertInteger)));
				break;
			}
			case 2:
			{
				System.out.println("Enter a number: ");
				int searchInteger = input.nextInt();
				if(search(T, searchInteger).item != -1)
				{
					System.out.println("Number Found");
				}
				else
				{
					System.out.println("Number Not Found");
				}
				System.out.println(search(T, searchInteger).item);
				break;
			}
			case 3:
			{
				System.out.println("Enter a number: ");
				int deleteInteger = input.nextInt();
				System.out.println(delete(T,deleteInteger));
				break;
			}
			case 4:
			{
				preOrder(T);
				break;
			}
			case 5:
			{
				inOrder(T);
				break;
			}
			case 6:
			{
				postOrder(T);
				break;
			}
			case 7:
			{
				again = -1;
				break;
			}
			
			}
		}
	}
	
	//Begin In Order Function
	private static void inOrder(BinarySearchTree<Integer> T) {
		if(T == null) return;
		inOrder(T.left);
		System.out.println(T.item);
		inOrder(T.right);
	}
	
	//Begin Pre Order Function
	private static void preOrder(BinarySearchTree<Integer> T) {
		if(T == null) return;
		System.out.println(T.item);
		inOrder(T.left);
		inOrder(T.right);
	}
	
	//Begin Post Order Function
	private static void postOrder(BinarySearchTree<Integer> T) {
		if(T == null) return;
		inOrder(T.left);
		inOrder(T.right);
		System.out.println(T.item);
		
	}
	
	//Begin Insert Function
	private static void insert(BinarySearchTree<Integer> T, BinarySearchTree<Integer> z)
	{
		BinarySearchTree<Integer> y = null;
		BinarySearchTree<Integer> x = T;
		while (x != null)
		{
			y = x;
			if(z.item < x.item)
			{
				x = x.left;
			}
			else
			{
				x = x.right;
			}
		}
		z.parent = y;
		if(y == null)
		{
			T.root = z;
		}
		else if(z.item < y.item)
		{
			y.left = z;
		}
		else
			y.right = z;
	}
	
	//Begin Search Function
	private static BinarySearchTree<Integer> search(BinarySearchTree<Integer> x, Integer i)
	{
		BinarySearchTree<Integer> y = new BinarySearchTree<Integer>(new Integer(-1));
		
		if(x.item == i.intValue())
		{
			return x;
		}
		else if (x.left == null && x.right == null)
		{
			return y;
		}
		else if(i < x.item)
		{
			return search(x.left, i);
		}
		else
		{
			return search(x.right, i);
		}

	}

	//Begin Delete Function
	private static String delete(BinarySearchTree<Integer> T, Integer i)
	{
		BinarySearchTree<Integer> y = null;
		BinarySearchTree<Integer> x = T;
		
		while(x != null)
		{
			if(i < x.item)
			{
				y = x;
				x = x.left;
			}
			else if(i > x.item)
			{
				y = x;
				x = x.right;
			}
			else
				break;
		}
		
		if(x == null)
		{
			return "Not Found";
		}
		
		if(x.left == null)
		{
			if(y == null)
			{
				T = x.right;
			}
			else
			{
				if(i < y.item)
				{
					y.left = x.right;
				}
				else
					y.right = x.right;
			}
		}
		else
		{
			BinarySearchTree<Integer> rightParent = x;
			BinarySearchTree<Integer> right = x.left;
			
			while(right.right != null)
			{
				rightParent = right;
				right = right.right;
			}
			
			x.item = right.item;
			
			if(rightParent.right == right)
			{
				rightParent.right = right.left;
			}
			else
				rightParent.left = right.left;
		}
		return "Element Deleted";	
		
	}
}
